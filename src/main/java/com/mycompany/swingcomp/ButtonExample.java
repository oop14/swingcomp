/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author Aritsu
 */
public class ButtonExample {
    ButtonExample(){    
        JFrame f=new JFrame("Button Example");            
        JButton b=new JButton(new ImageIcon("D:\\icon.png"));    
        b.setBounds(100,100,100, 40);    
        f.add(b);       
        f.setSize(300,400);    
        f.setLayout(null);    
        f.setVisible(true);    
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
    }         
    
    public static void main(String[] args) {
        ButtonExample ex = new ButtonExample();
    }
}